package ru.kev.regularExpressions;

import java.io.*;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;


/**
 * Этот класс позволяет из произвольной программы на языке java удалить многострочные и
 * однострочные комментарии и лишние пробелы(переносы строки, табуляцию и т.д.).
 * Также этот класс находит все идентификаторы из полученной программы
 * и записывает их в отдельный файл.
 *
 * @author Kotelnikova E.V. group15oit20
 */

public class RegularExpressions {
    //static final String pathToFile = "D://Учёба//ОП//RegularExpressions//src//ru//kev//regularExpressions//RegularExpressions.java";
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите путь к файлу: ");
        String pathToFile = scanner.next();
        StringBuilder file = new StringBuilder();

        file.append(readFile(pathToFile));

        file = deleteComment(file, "/*", "*/");
        // file = deleteComment(file, "\"", "\"");
        file = deleteComment(file, "//", "\n");

        file = deleteSpace(file);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("file.java"))) {
            writer.write(file.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        findIdentifier();
    }

    /**
     * Метод для поиска идентификаторов
     */
    public static void findIdentifier() {
        String s;
        Pattern pattern = Pattern.compile("[a-z_]\\w*", CASE_INSENSITIVE); // w - буквенный, цифровой символ или знак подчеркивания
        Matcher matcher = pattern.matcher("");
        try (BufferedReader reader = new BufferedReader(new FileReader("file.java"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("identifier.txt"))) {
            s = reader.readLine();
            //s = s.replaceAll("[\"](.*?)[\"]","");
            matcher.reset(s);
            while (matcher.find()) {
                System.out.println(matcher.group());
                writer.write(matcher.group() + "\n");
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Метод для удаления комментариев
     *
     * @param file     значение исходного файла
     * @param beginInd начальный индекс, с которого будет происходить удаление комментария
     * @param endInd   конечный индекс, до которого будет происходить удаление комментария
     * @return переменную типа StringBuilder, которая не содержит комментариев
     */
    public static StringBuilder deleteComment(StringBuilder file, String beginInd, String endInd) {
        int i;
        int j;
        while ((i = file.indexOf(beginInd)) >= 0 & (j = file.indexOf(endInd, i + 1)) > 0) {
            file.delete(i - 1, j + 3);

        }
        return file;
    }


    /**
     * Метод для удаления лишних пробелов из файла
     *
     * @param file
     * @return
     */
    public static StringBuilder deleteSpace(StringBuilder file) {
        String s;
        s = file.toString();
        s = s.replaceAll("\\s+", " ");
        file.setLength(0);
        file.append(s);
        return file;

    }

    /**
     * Метод читает значение из файла и записывает в StringBuilder
     *
     * @param pathToFile путь к файлу
     * @return переменную типа StringBuilder, которая содержит значение из файла
     */

    public static StringBuilder readFile(String pathToFile) {
        StringBuilder file = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToFile))) {
            String s;
            while ((s = reader.readLine()) != null) {
                file.append(s + "\n");
            }
        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
        } catch (IOException e) {
            System.err.println("Ошибка ввода-вывода данных");
        }
        return file;
    }
}


